﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DashifyClipboardTrayApp.Helpers
{
    public static class ClipboardManager
    {
        public static string GetTextFromClipboard()
        {
            string text = "";
            try
            {
                text = Clipboard.GetText();
            }
            catch (Exception ex)
            {
                ExceptionHandler.Handle(ex);
            }
            return text;
        }

        public static void SetTextToClipboard(string text)
        {
            try
            {
                Clipboard.SetText(text);
            }
            catch (Exception ex)
            {
                ExceptionHandler.Handle(ex);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace DashifyClipboardTrayApp.Models
{
    public static class HotKeyManager
    {
        // Constants for Windows API functions
        private const int WM_HOTKEY = 0x0312;
        private const int MOD_CONTROL = 0x0002;
        private const int MOD_SHIFT = 0x0004;
        private const int MOD_ALT = 0x0001;

        // Windows API function to register hotkey
        [DllImport("user32.dll")]
        private static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vk);

        // Windows API function to unregister hotkey
        [DllImport("user32.dll")]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        // Define a unique identifier for the hotkey
        private const int HOTKEY_ID = 1;

        // Event that will be triggered when the hotkey is pressed
        public static event EventHandler? HotkeyPressed;

        // Method to register the hotkey
        public static void RegisterHotkey(IntPtr handle)
        {
            // Register the hotkey using Ctrl+Shift+Alt+'Num-' or just '-'
            RegisterHotKey(handle, HOTKEY_ID, (MOD_CONTROL | MOD_SHIFT | MOD_ALT), (int)Keys.Subtract);
        }

        // Method to unregister the hotkey
        public static void UnregisterHotkey(IntPtr handle)
        {
            // Unregister the hotkey
            UnregisterHotKey(handle, HOTKEY_ID);
        }

        // Method to process the hotkey message
        public static void ProcessHotkey(Message message)
        {
            // Check if the message is a hotkey message
            if (message.Msg == WM_HOTKEY && (int)message.WParam == HOTKEY_ID)
            {
                // Trigger the HotkeyPressed event
                HotkeyPressed?.Invoke(null, EventArgs.Empty);
            }
        }
    }
}

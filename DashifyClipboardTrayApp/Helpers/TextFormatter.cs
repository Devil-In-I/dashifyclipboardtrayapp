﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DashifyClipboardTrayApp.Helpers
{
    public static class TextFormatter
    {
       public static string DashifyText(string text)
        {
            if (text == null)
            {
                throw new ArgumentNullException(nameof(text),"Text parameter must not be null.");
            }
            return text.Trim().Replace(' ', '-');
        }
    }
}

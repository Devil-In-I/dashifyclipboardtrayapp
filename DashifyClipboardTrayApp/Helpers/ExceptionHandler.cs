﻿namespace DashifyClipboardTrayApp.Helpers
{
    public static class ExceptionHandler
    {
        /// <summary>
        /// Informs the user with messageBox about unexpected error, showing the information about exception that was caught.
        /// </summary>
        /// <param name="exception">Catched exception to handle</param>
        public static void Handle(Exception exception)
        {
            string errorMessage = "Some error just appered: " + exception.Message;

            string detailedMessage = "Additional information about the error:\n" + exception.StackTrace;

            string calmMessage = "Application is still running and nothing will happen with your data. You don't have to worry";

            var lineBrake = $"{Environment.NewLine}{Environment.NewLine}";

            string fullMessage = $"{errorMessage}{lineBrake}{detailedMessage}{lineBrake}{calmMessage}";

            MessageBox.Show(fullMessage, "Unexpected error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}

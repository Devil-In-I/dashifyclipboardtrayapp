﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace DashifyClipboardTrayApp.Models
{
    public class TrayApplicationContext : ApplicationContext
    {
        private readonly NotifyIcon trayIcon;

        public TrayApplicationContext()
        {
            trayIcon = new NotifyIcon()
            {
                Icon = new Icon(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\..\Resources\Icons\icon.ico")),
                Text = "Dashify Clipboard Tray App",
                Visible = true
            };

            var contextMenu = new ContextMenuStrip();
            var exitMenuItem = new ToolStripMenuItem("Exit");
            var stopWaitingShortcutMenuItem = new ToolStripMenuItem("Stop awaiting shortcut");
            var startWaitingShortcutMenuItem = new ToolStripMenuItem("Start awaiting shortcut");
            var dashifyMenuItem = new ToolStripMenuItem("Dashify Text");
            exitMenuItem.Click += ExitMenuItem_Click;
            stopWaitingShortcutMenuItem.Click += StopWaitingShortcutMenuItem_Click;
            startWaitingShortcutMenuItem.Click += StartWaitingShortcutMenuItem_Click;
            dashifyMenuItem.Click += DashifyMenuItem_KeyPressed;

            contextMenu.Items.Add(exitMenuItem);
            contextMenu.Items.Add(stopWaitingShortcutMenuItem);
            contextMenu.Items.Add(startWaitingShortcutMenuItem);
            contextMenu.Items.Add(dashifyMenuItem);
            trayIcon.ContextMenuStrip = contextMenu;

            // Register the global hotkey
            HotKeyManager.RegisterHotkey(IntPtr.Zero); // IntPtr.Zero represents the whole application
            // Subscribe to the HotkeyPressed event
            HotKeyManager.HotkeyPressed += KeyboardHook_KeyPressed;

            Application.ApplicationExit += Application_ApplicationExit;
            Application.Idle += Application_Idle;
        }

        protected override void Dispose(bool disposing)
        {
            HotKeyManager.UnregisterHotkey(IntPtr.Zero); // IntPtr.Zero represents the whole application
            base.Dispose(disposing);
        }

        private void Application_ApplicationExit(object? sender, EventArgs e)
        {
            // Make sure to clean up and dispose the tray icon when the application exits
            trayIcon.Visible = false;
            trayIcon.Dispose();
        }

        private void Application_Idle(object? sender, EventArgs e)
        {
            // Run the application message loop to handle tray icon messages
            Application.DoEvents();
        }

        private void KeyboardHook_KeyPressed(object? sender, EventArgs e)
        {
            ClipboardTextProcessor.ProcessTextFromClipboard();
        }

        private void DashifyMenuItem_KeyPressed(object? sender, EventArgs e)
        {
            ClipboardTextProcessor.ProcessTextFromClipboard();
        }

        private void ExitMenuItem_Click(object? sender, EventArgs e)
        {
            // Обработчик выхода из приложения
            trayIcon.Visible = false;
            Application.Exit();
        }

        private void StopWaitingShortcutMenuItem_Click(object? sender, EventArgs e)
        {
            HotKeyManager.UnregisterHotkey(IntPtr.Zero); // IntPtr.Zero represents the whole application
        }

        private void StartWaitingShortcutMenuItem_Click(object? sender, EventArgs e)
        {
            HotKeyManager.RegisterHotkey(IntPtr.Zero); // IntPtr.Zero represents the whole application
        }
    }
}

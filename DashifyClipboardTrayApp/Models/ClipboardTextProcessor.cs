﻿using DashifyClipboardTrayApp.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace DashifyClipboardTrayApp.Models
{
    public static class ClipboardTextProcessor
    {
        public static void ProcessTextFromClipboard()
        {
            var text = ClipboardManager.GetTextFromClipboard();

            text = text.ReplaceSpacesWithDashes();

            ClipboardManager.SetTextToClipboard(text);
        }

        private static string ReplaceSpacesWithDashes(this string text)
        {
            try
            {
                text = TextFormatter.DashifyText(text);
            }
            catch (Exception ex)
            {
                ExceptionHandler.Handle(ex);
            }
            return text;
        }
    }
}
